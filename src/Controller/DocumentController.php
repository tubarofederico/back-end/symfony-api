<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/api", name="version_1_")
 */
class DocumentController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine){
        $this->doctrine = $doctrine;
    }


    protected function getDocumentoFromBD(int $id_documento = null) {
        $conn = $this->doctrine->getConnection();

        $query = "EXEC DyFDigital.dbo.sp_buscar_docs_por_rol_usuario_destino @id_usuario = ? , @id_documento = ?";
        $stmt = $conn->prepare($query);
        $stmt->bindValue(1, $this->getUser()->getId());
        $stmt->bindValue(2, $id_documento);
        $stmt->execute();

        $doc = $stmt->fetchAssociative(); // Obtengo el documento

        return $doc;
    }

    protected function getDocumentos(int $id_documento = null): Array {
        $conn = $this->doctrine->getConnection();

        $query = "EXEC DyFDigital.dbo.sp_buscar_docs_por_rol_usuario_destino @id_usuario = ?, @id_documento = ?";
        $stmt = $conn->prepare($query);
        $stmt->bindValue(1, $this->getUser()->getId());
        $stmt->bindValue(2, $id_documento);
        $stmt->execute();

        $docs = $stmt->fetchAllAssociative();
        $newDocs = [];
        foreach ($docs as $doc) {
            if(@file_exists("../../".$doc["nombre_pdf"])){
                $newDocs[] = [
                    "id_documento" => $doc["id_documento"],
                    "nombre_pdf" => $doc["nombre_pdf"],
                    "protocolizar" => (bool) $doc["ultimo_paso"]
                ];
            }
        }

        return $newDocs;
    }

    /**
     * @Route("/document", name="listadoDocumentos", methods={"GET"})
     */
    public function listadoDocumentos(Request $request): JsonResponse {

        $docs = $this->getDocumentos();

        return $this->json($docs);
    }

    /**
     * @Route("/document/{id_documento}/protocolizacion", name="obtener_numero_protocolizacion", methods={"GET"})
     */
    public function obtenerNumeroDeProtocolizacion(int $id_documento, Request $request): JsonResponse {
        
        // if(!$this->getUser()->isMe($id_usuario)){
        //     throw new AccessDeniedHttpException('El usuario por el que se desea consultar no cincide con el usuario autenticado.');
        // }

        $docs = $this->getDocumentos($id_documento);
        
        if(count($docs) == 1){
            $doc = $docs[0];
            if(@file_exists("../../".$doc["nombre_pdf"])){
                $nroResolucion = rand(10000, 20000);
            }

            return $this->json([
                "numero" => $nroResolucion
            ]);
        }
          
        return $this->json(["mensaje" => "No se pudo obtener un número de resolución para el documento."],404);
    }

    /**
     * @Route("/document/{id_documento}", name="getDocumento", methods={"GET"})
     */
    public function getDocumento(
        int $id_documento,
        Request $request
    ): JsonResponse {

        // if(!$this->getUser()->isMe($id_usuario)){
        //     throw new AccessDeniedHttpException('El usuario por el que se desea consultar no cincide con el usuario autenticado.');
        // }

        $docs = $this->getDocumentos($id_documento);
        
        if(count($docs) == 1){
            $doc = $docs[0];
            $newDoc['id_documento'] = $doc["id_documento"];
            $newDoc['protocolizar'] = $doc["protocolizar"];
            $newDoc['protocolizacion'] = [
                "protocolizar" => $doc["protocolizar"],
                "datos" => [
                    "numero" => true,
                    "fecha" => true
                ],
            ];
            $newDoc["base64"] = null;

            $file = @file_get_contents("../../".$doc["nombre_pdf"]);
            if($file != false){
                $base64 = base64_encode($file);

                $newDoc["base64"] = $base64;

                return $this->json($newDoc);
            }
            
        }

        return $this->json(["mensaje" => "No se encontro el documento en el directorio."],404);
    }

    /**
     * @Route("/document/{id_documento}", name="actualizarYMoverDocumento", methods={"PUT"})
     */
    public function actualizarYMoverDocumento(
        int $id_documento,
        Request $request
    ): JsonResponse {

        $doc = $this->getDocumentoFromBD($id_documento); // Obtengo el documento

        if($doc == null){
            return $this->json(["mensaje" => "El número de documento no es válido."],404);
        }

        // Body param para saber si hay que protocolizar el documento estableciendo el numero de resolución
        // Debemos consultar como tenemos que hacerlo
        $numero = null;
        if(array_key_exists("numero", $request->toArray())){
            $numero = $request->toArray()["numero"];
        }

        if($doc["ultimo_paso"] == 1 && $numero == null){
            return $this->json(["mensaje" => "El documento requiere protocolarización, indique el número de resolucion."],400);
        }

        // Guardo el nuevo documento recibido por parámetro en el directorio destino del movimiento.
        $base64String = $request->toArray()["base64"];
        $dir_dest = realpath($_SERVER['DOCUMENT_ROOT']) . "/" . $doc['directorio_destino'];
        // $dest = $dir_dest . "/" . $doc['nombre_pdf_mostrar']; // "C:\wamp64\www/FIRMADIGITAL/AUDITORIA/REVISOR/RE_2024-10.pdf"
        $dest = $dir_dest . "/" . $this->getNombreArchivo($doc, $numero); // "C:\wamp64\www/FIRMADIGITAL/AUDITORIA/REVISOR/RE_2024-10.pdf"
        $data = base64_decode($base64String);
        $file = fopen($dest, 'wb'); // Abre el archivo en modo de escritura binaria
        $result = fwrite($file, $data); // Escribe los datos en el archivo
        fclose($file); // Cierra el archivo
        
        if($result != false){
            $conn = $this->doctrine->getConnection();
            // Si se guardo el nuevo archivo realizo el movimiento por Store Procedure
            $query = "EXEC DyFDigital.dbo.sp_documentos_movimientos_graba @accion = ?, @id_documento_movimiento = ?, @id_documento = ?, @f_movim = ?,
            @id_circuito_paso = ?, @id_depen_origen = ?, @id_depen_destino = ?, @estado = ? , @clave = ?, @viene_de = ?, @periodo = ?, @id_rol_o = ?,
            @id_rol_d = ?, @mensaje_out = ?, @id_doc_mov_out = ?, @conlink = ?, @nro_remito = ?";
            $stmt = $conn->prepare($query);
            $stmt->bindValue(1, "A");   // @accion
            $stmt->bindValue(2, 0); // @id_documento_movimiento
            $stmt->bindValue(3, $doc["id_documento"]); // @id_documento
            $stmt->bindValue(4, date("Y-m-d H:i:s")); // @f_movim (Today)
            $stmt->bindValue(5, $doc["id_proximo_paso"]); // @id_circuito_paso
            $stmt->bindValue(6, $doc["id_depen_origen"]); // @id_depen_origen
            $stmt->bindValue(7, $doc["id_depen_destino"]); // @id_depen_destino
            $stmt->bindValue(8, $doc["estado"]); // @estado
            $stmt->bindValue(9, $this->getUser()->getId()); // @clave
            $stmt->bindValue(10, "M"); // @viene_de
            $stmt->bindValue(11, $doc["periodo"]); // @periodo
            $stmt->bindValue(12, $doc["id_rol_origen"]); // @id_rol_o
            $stmt->bindValue(13, $doc["id_rol_destino"]); // @id_rol_d
            $stmt->bindValue(14, ""); // @mensaje_out
            $stmt->bindValue(15, 0); // @id_doc_mov_out
            $stmt->bindValue(16, $doc["conLink"]); // @conlink
            $stmt->bindValue(17, $doc["nro_remito"]); // @nro_remito
            $stmt->execute();

            $rta = $stmt->fetchAssociative();

            if($rta["resultado"] == 1){
                return $this->json(["menssage" => $rta["mensaje"]], 500);
            } else {
                // Elimino el archivo del origen.
                $dir_origen = realpath($_SERVER['DOCUMENT_ROOT']) . "/" . $doc['directorio_origen'];
                $origen = $dir_origen . "/" . $doc['nombre_pdf_mostrar']; // "C:\wamp64\www/FIRMADIGITAL/AUDITORIA/REVISOR/RE_2024-10.pdf"
                if(@unlink($origen)){
                    return $this->json(["menssage" => "Se ha realizado correctamente el movimiento del documento."],201);
                }
            }

        }

        return $this->json(["menssage" => "Ha ocurrido un error al mover el documento."],500);
    }

    private function getNombreArchivo($doc, $numero){
        // $conn = $this->doctrine->getConnection();

        // $query = "EXEC DyFDigital.dbo.sp_devuleve_nombre_archivo_output @id_documentos_tipo = ?, @periodo = ?, @numero = ?, @expte = ?, @nombre_archivo = ?";
        // $stmt = $conn->prepare($query);
        // $stmt->bindValue(1, $doc["id_documentos_tipo"]);
        // $stmt->bindValue(2, $doc["periodo"]);
        // $stmt->bindValue(3, $numero);
        // $stmt->bindValue(4, $doc["clave_exp"]);
        // $stmt->bindValue(5, $doc["nombre_pdf_mostrar"]);
        // $stmt->execute();

        // $rta = $stmt->fetchAssociative(); // Obtengo el documento

        return $doc["nombre_pdf_mostrar"];
    }

    /**
     * @Route("/document/{id_documento}/preview", name="previsualizarDocumento", methods={"GET"})
     */
    public function previzualizarDocumento(int $id_documento, Request $request) {
        
        $doc = $this->getDocumentoFromBD($id_documento); // Obtengo el documento

        if($doc == null){
            return $this->json(["mensaje" => "El número de documento no es válido."],404);
        }

        $file = @file_get_contents("../../".$doc["nombre_pdf"]);
        if($file != false){
            
            // Set the header
            header('Content-Length: ' . strlen($file));
            header('Content-Type: application/pdf');
            header('Content-disposition: filename="' . $doc["nombre_pdf_mostrar"] . '"');

            // Echo the data
            echo $file;

            die;
        }

        return $this->json("No existe el documento solicitado");
    }
}
