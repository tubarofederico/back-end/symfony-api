<?php
// src/Security/UserProvider.php
namespace App\Security;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Security\User;

class UserProvider implements UserProviderInterface
{
    private $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Symfony calls this method if you use features like switch_user
     * or remember_me. If you're not using these features, you do not
     * need to implement this method.
     *
     * @throws UserNotFoundException if the user is not found
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        //CHEQUEO DE USUARIO EN LA INTRANET
        $usuarioSinEnie = str_replace("Ñ", "N", $identifier);
        $usuarioSinEnie = str_replace("ñ", "n", $usuarioSinEnie);
        $stmt = $this->registry->getConnection("seguridad")->prepare("exec sp_usuario_validar_login_LDAP @vid_usuario = ? ");
        $stmt->bindValue(1, $usuarioSinEnie);
        $stmt->execute();
        $datosUser = $stmt->fetch();

        if(!empty($datosUser)){

            return new User($datosUser);

        }     

        throw new UserNotFoundException("Usuario NO encontrado en la base de datos", 1);

        // Load a User object from your data source or throw UserNotFoundException.
        // The $identifier argument is whatever value is being returned by the
        // getUserIdentifier() method in your User class.
        // throw new \Exception('TODO: fill in loadUserByIdentifier() inside '.__FILE__);
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * When a user is logged in, at the beginning of each request, the
     * User object is loaded from the session and then this method is
     * called. Your job is to make sure the user's data is still fresh by,
     * for example, re-querying for fresh User data.
     *
     * If your firewall is "stateless: true" (for a pure API), this
     * method is not called.
     */
    public function refreshUser(UserInterface $user): UserInterface
    {

        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        // Return a User object after making sure its data is "fresh".
        // Or throw a UserNotFoundException if the user no longer exists.
        throw new \Exception('TODO: fill in refreshUser() inside '.__FILE__);
    }

    /**
     * Tells Symfony to use this provider for this User class.
     */
    public function supportsClass(string $class): bool
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        // Implement your logic to load a user by their username (e.g., from a database)
        // Return an instance of your User class that implements UserInterface
        return $this->loadUserByIdentifier($username);
    }
}