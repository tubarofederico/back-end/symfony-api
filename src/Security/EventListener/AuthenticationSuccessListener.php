<?php
namespace App\Security\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        // Customize your response here
        $data["clave"] = $user->getId(); 
        $data["nombre"] = $user->getNombreParaFirma(); 
        $data["cargo"] = $user->getCargo(); 

        $event->setData($data);
    }
}
