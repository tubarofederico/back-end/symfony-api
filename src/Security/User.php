<?php

// src/Security/User.php
namespace App\Security;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{

    private $id;

    private $apellido;

    private $nombres;

    private $c_dependen;

    private $d_dependen;
    private $c_dependen_N;
    private $d_dependen_N;
    private $email;
    private $grupo_usu;
    private $grupo;

    private $roles = [];

    private $password;

    public function __construct($dataBaseUserResponse){
        $this->id = $dataBaseUserResponse["id_usuario"];
        $this->apellido = $dataBaseUserResponse["apellido"];
        $this->nombres = $dataBaseUserResponse["nombres"];
        $this->c_dependen = $dataBaseUserResponse["c_dependen"];
        $this->d_dependen = $dataBaseUserResponse["d_dependen"];
        $this->c_dependen_N = $dataBaseUserResponse["c_dependen_N"];
        $this->d_dependen_N = $dataBaseUserResponse["d_dependen_N"];
        $this->email = $dataBaseUserResponse["email"];
        $this->grupo_usu = $dataBaseUserResponse["grupo_usu"];
        $this->grupo = $dataBaseUserResponse["grupo"];
        $this->roles = ["ROLE_USER"];
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * The public representation of the user (e.g. a username, an email address, etc.)
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->getId();
    }

    /**
     * @deprecated since Symfony 5.3
     */
    public function getUsername(): string
    {
        return (string) $this->getId();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        // $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        // No es utilizado
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        // No es utilizado
        $this->password = $password;

        return $this;
    }

    public function getNombreParaFirma(): ?string
    {
        return trim($this->apellido) . " " . trim($this->nombres);
    }

    public function getCargo(): ?string
    {
        return "Vicepresidente";
    }

    /**
     * Returning a salt is only needed if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}